# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name     = "flounder"
  s.version  = '1.0.1'
  s.summary  = "Flounder is a way to write SQL simply in Ruby. It deals with everything BUT object relational mapping. "
  s.email    = "kaspar.schiess@technologyastronauts.ch"
  s.homepage = "https://bitbucket.org/technologyastronauts/oss_flounder"
  s.authors  = ['Kaspar Schiess', 'Florian Hanke']
  
  s.description = <<-EOF
    Flounder is the missing piece between the database and your Ruby code. It 
    allows very simple access to database tables without going all the way to
    begin an object mapper. 
  EOF

  s.add_runtime_dependency 'arel', '~> 5', '> 5.0.1'
  s.add_runtime_dependency 'pg', '~> 0.17'
  s.add_runtime_dependency 'connection_pool', '~> 2'
  s.add_runtime_dependency 'pg-hstore', '~> 1.2', '>= 1.2.0'
  s.add_runtime_dependency 'aggregate', '~> 0.2', '>= 0.2.2'
  
  s.files         = Dir['**/*']
  s.test_files    = Dir['qed/**/*']
  s.require_paths = ["lib"]

  s.licenses      = 'MIT'
end
