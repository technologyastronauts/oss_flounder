
# First benchmark just tests loading of data into array format. 
# Run with 
#   ruby -r profile benchmark/001.rb

require_relative 'lib/connection'

domain = Benchmark.domain
perf01 = domain[:perf01]

a = perf01.all.to_a

