
# First benchmark just tests loading of data into array format. 

require_relative 'lib/connection'
require 'ruby-prof'

domain = Benchmark.domain
perf01 = domain[:perf01]

r = perf01.all.to_a
RubyProf.start
a = r.map(&:foo).inject(0) { |sum, el| sum + el }
result = RubyProf.stop

# p a

printer = RubyProf::DotPrinter.new(result)
printer.print(STDOUT, min_percent: 2)