
require_relative 'lib/connection'
require_relative 'lib/random'

domain = Benchmark.domain
perf01 = domain[:perf01]

n = 1000
puts "Loading perf01 with #{n} records..."
n.times do |n|
  perf01.insert(
    foo: n, 
    bar: rand(100000), 
    baz: rand(1000), 
    when: Time.now, 
    count: n,
    iif: rand() < 0.5, 
    type: Benchmark.rand_string(5, 25), 
    bemerkung: Benchmark.rand_string(5, 200)).kick
end


p perf01.all.to_a