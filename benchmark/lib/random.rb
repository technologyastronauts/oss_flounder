
module Benchmark

module_function
  def rand_string min_len, max_len
    len = min_len + rand(max_len-min_len)

    s = ''
    len.times do
      s << (?a.ord + rand(27)).chr
    end

    s
  end
end