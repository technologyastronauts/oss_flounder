
# To get this to work, please run the qed tests first, since those load the
# database structure. 

require 'flounder'

module Benchmark

module_function 
  def domain
    @domain ||= begin
      connection = Flounder.connect(dbname: 'flounder')
      Flounder.domain(connection) do |dom|
        dom.entity(:perf01, :perf01, 'perf01')
      end
    end
  end
end

