
Flounder can delete records from the database.

~~~ruby
  post = posts.insert(title: 'test', text: 'text', user_id: 1).kick.first

  posts.delete.where(id: post.id).kick

  posts.where(id: post.id).count.assert == 0
~~~