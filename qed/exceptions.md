
Some things are forbidden. Here's a list of those: 

# Invalid Field References

All of these statements raise an `InvalidFieldReference` exception.

~~~ruby
  expect Flounder::InvalidFieldReference do
    users.project(1, 2, 3)
  end
~~~

# Double projection

You cannot have the same field name twice in a result. 

~~~ruby
  expect Flounder::DuplicateField do
    users.project('id, id').all
  end
~~~

# Bind Index out of Bounds

Indices of bind expressions in `#where` need to be relative to the argument list given. 

~~~ruby
  expect Flounder::BindIndexOutOfBounds do
    users.where('id = $100', 1)
  end
~~~

