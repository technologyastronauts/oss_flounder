An insert creates a state from which SQL can be extracted.

~~~ruby
  sql = users.insert(:name => 'Mr. Insert SQL').to_sql

  sql.assert == "INSERT INTO \"users\" (\"name\") VALUES ($1) RETURNING *"
~~~

Using returning will return the inserted object.

~~~ruby
  results = users.insert(:name => 'Mr. Returning Asterisk').kick

  results.first.name.assert == 'Mr. Returning Asterisk'
~~~

Returning all fields is the default, but you can provide that explicitly.

~~~ruby
  results = users.insert(:name => 'Mr. Returning Asterisk').returning('*').kick

  results.first.name.assert == 'Mr. Returning Asterisk'
~~~

Using returning with a field name will return those fields of the inserted object.

~~~ruby
  results = users.insert(:name => 'Mr. Returning').returning(:id).kick

  id = results.first.id

  users.where(:id => id).first.name.assert == 'Mr. Returning'
~~~

Flounder fields can be used as keys.

~~~ruby
  results = users.insert(users[:name] => 'Mr. Flounder Field').kick

  results.first.name.assert == 'Mr. Flounder Field'
~~~

## Special Types

### HSTORE

Flounder supports fields in hstore format through the `pg-hstore` gem.

~~~ruby
  row = users.insert(name: 'HStore User', attributes: {foo: 'bar'}).kick.first

  row = users.where(id: row.id).first
  row.attributes.assert == {'foo' => 'bar'}
~~~