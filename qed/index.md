
Flounder is a simple layer between you and the database. It abstracts syntax details and removes the need for string manipulation and parsing, but it does not 'map to objects' in the traditional sense. It still returns objects from queries, though - and it certainly allows influencing the mapping, but the core of flounder is about making querying and manipulating the database look nice and be maintainable. Here are our guiding principles: 

* Given the choice, we'll chose the method name close to SQL. Flounder should make that domain knowledge more useful, not introduce another domain. 
* Composability over complexity.

# A simple Domain

Here's our domain definition. In which - yes - you specify both singular and plural variants for each entity. Once. 

~~~ruby  
  connection = Flounder.connect(dbname: 'flounder')
  domain = Flounder.domain(connection) do |dom|
    dom.entity(:users, :user, 'users')
    dom.entity(:posts, :post, 'posts')
    dom.entity(:comments, :comment, 'comments')
  end
~~~

Now very simple selects should work as expected.

~~~ruby
  domain[:users].where(id: 1).assert generates_sql(
    %Q(SELECT [fields] FROM "users"  WHERE "users"."id" = 1))
~~~

The `domain[:users]` bit refers to a relation in the database and when you append another square bracket pair, you'll refer to a field in the database - everywhere, in all flounder clauses. 

~~~ruby
  domain[:users][:id].assert.kind_of? Flounder::Field
~~~

Also, several conditions work as one would expect from DataMapper. 

~~~ruby
  domain[:users].where(id: 1..10).assert generates_sql( 
    %Q(SELECT [fields] FROM "users"  WHERE "users"."id" BETWEEN 1 AND 10))

  domain[:users].where(:id.lt => 10).assert generates_sql( 
    "SELECT [fields] FROM \"users\"  WHERE \"users\".\"id\" < 10")
  domain[:users].where(:id.lteq => 10).assert generates_sql( 
    "SELECT [fields] FROM \"users\"  WHERE \"users\".\"id\" <= 10")
  domain[:users].where(:id.gt => 10).assert generates_sql( 
    "SELECT [fields] FROM \"users\"  WHERE \"users\".\"id\" > 10")
  domain[:users].where(:id.gteq => 10).assert generates_sql( 
    "SELECT [fields] FROM \"users\"  WHERE \"users\".\"id\" >= 10")
  domain[:users].where(:id.not_eq => 10).assert generates_sql( 
    "SELECT [fields] FROM \"users\"  WHERE \"users\".\"id\" != 10")
~~~

Fields can be used fully qualified by going through the entity.

~~~ruby
  domain[:users].where(domain[:users][:id] => 10).
    assert generates_sql(%Q(SELECT [fields] FROM "users"  WHERE "users"."id" = 10))

  domain[:users].where(domain[:users][:name].matches => 'a%').
    assert generates_sql(%Q(SELECT [fields] FROM "users"  WHERE "users"."name" ILIKE 'a%'))
~~~

~~~ruby
  domain[:users].where(:user_id => :approver_id).
  assert generates_sql("SELECT [fields] FROM \"users\"  WHERE \"users\".\"user_id\" = \"users\".\"approver_id\"")
~~~

By default, symbols are interpreted as field names in the entity that you start your query with. But you can localize your `#where` clause to any other entity. 

~~~ruby
  users.where(posts, :id => 1).
    assert generates_sql("SELECT [fields] FROM \"users\"  WHERE \"posts\".\"id\" = 1")
  users.where(:posts, :id => 1).
    assert generates_sql("SELECT [fields] FROM \"users\"  WHERE \"posts\".\"id\" = 1")
~~~

Sets are queried for using the `IN` operator.

~~~ruby
  require 'set'
  s = Set.new([1,2,3])
  users.where(:posts, :id.in => s).
    assert generates_sql("SELECT [fields] FROM \"users\"  WHERE \"posts\".\"id\" IN (1, 2, 3)")
  users.where(:posts, :id => s).
    assert generates_sql("SELECT [fields] FROM \"users\"  WHERE \"posts\".\"id\" IN (1, 2, 3)")
~~~

TODO links to other documentation

