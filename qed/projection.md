
Without projection, result objects will be packaged in a subhash.

~~~ruby
  user = posts.join(users).on(:user_id => :id).
    first.user

  user.id.assert != nil
  user.name.assert == 'John Snow'
~~~

When projecting, all result data will be toplevel and only the projected data is loaded.

~~~ruby
  domain[:users].where(id: 2013).project(domain[:users][:id]).
    to_sql.assert == %Q(SELECT "users"."id" FROM "users"  WHERE "users"."id" = 2013)

  result = posts.join(users).on(:user_id => :id).
    project(users[:name]).first

  result.refute.respond_to?(:id) # Not loaded.

  result.refute.id? # Synonymous to obj.respond_to?(:id) && !obj.id.nil?
  result.user.assert.name?
  
  result.user.name.assert == 'John Snow'
~~~

Otherwise a projected query can be treated like an ordinary query, including appending stuff by joining it in. 

~~~ruby
  post = posts.
    join(users).on(:user_id => :id).
    join(users.as(:approvers, :approver)).on(:approver_id => :id).
    first

  post.user.name.assert == 'John Snow' 
  post.approver.name.assert == 'John Doe' 
~~~

The same (by name) field can be used from multiple tables without issues.

~~~ruby
  result = posts.
    join(users).on(:user_id => :id).
    project(users[:id], users[:name], posts[:id], posts[:title]).
    first

  result.title.assert == "First Light"
  result.post.title.assert == "First Light"
  result.user.name.assert == 'John Snow'
~~~

Fields that are created during the select are toplevel as well. 

~~~ruby
  result = posts.project('42 as answer').first
  result.answer.assert = 42
~~~

