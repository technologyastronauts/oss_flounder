
# Basic JOINs
Joins are correctly created.

~~~ruby
  query = domain[:posts].
    join(users).on(:user_id => :id)

  query.assert generates_sql(%Q(SELECT [fields] FROM "posts" INNER JOIN "users" ON "posts"."user_id" = "users"."id"))

  row = query.first
  row.post.id.assert == 1
~~~

Join conditions are joined using `AND`. 

~~~ruby
  query = domain[:posts].
    join(users).on(:user_id => :id, :strange_example_id => :id)

  query.assert generates_sql(%Q(SELECT [fields] FROM "posts" INNER JOIN "users" ON "posts"."user_id" = "users"."id" AND "posts"."strange_example_id" = "users"."id"))
~~~

You might want to drop the calls to domain. 

~~~ruby 
  domain[:users].join(:posts).on(:id => :user_id).
    assert generates_sql(
      %Q(SELECT [fields] FROM "users" INNER JOIN "posts" ON "users"."id" = "posts"."user_id"))
~~~


# Anchoring

Joining presents an interesting dilemma. There are two ways of joining things together, given three tables. The sequence A.B.C might mean to join A to B and C; it might also be interpreted to mean to join A to B and B to C. Here's how we solve this. 

~~~ruby
  domain[:users].
    join(domain[:posts]).on(:id => :user_id).
    join(domain[:comments]).on(:id => :post_id).
      assert generates_sql(%Q(SELECT [fields] FROM "users" INNER JOIN "posts" ON "users"."id" = "posts"."user_id" INNER JOIN "comments" ON "users"."id" = "comments"."post_id"))
~~~

So just doing `A.B.C` will give you the first of the above possibilities. Here's how to achive the second effect. 

~~~ruby
  users.
    join(:posts).on(:id => :user_id).anchor.
    join(:comments).on(:id => :post_id).
      assert generates_sql(%Q(SELECT [fields] FROM "users" INNER JOIN "posts" ON "users"."id" = "posts"."user_id" INNER JOIN "comments" ON "posts"."id" = "comments"."post_id"))
~~~

During the construction of the query, flounder keeps track of what table joins are relative to. That table also provides field resolution for the left hand side of the `#on` clause: 

    A.join(B).on(:a_field => :b_field).
      join(C).on(:a_field => :c_field)

By default, A in the above example will always be the entity that you started the query with. To anchor a join on a different entity, you will need to join that entity in and then end the join with a call to `#anchor`: 

    A.join(B).on(:a_field => :b_field).anchor.
      join(C).on(:b_field => :c_field)

To get rid of the anchor, you will need to call `#hoist`. This will always reset the left side of all further joins to the entity that you started the 
query with:

    A.join(B).on(:a_field => :b_field).anchor.
      join(C).on(:b_field => :c_field).hoist.
      join(D).on(:a_field => :d_field)

Here's a real usage example.

~~~ruby
  query = users.
    join(:posts).on(:id => :user_id).anchor.
    join(:comments).on(:id => :post_id).hoist.
    join(comments.as(:other_comments, :other_comment)).on(:id => :author_id)

  query.assert generates_sql("SELECT [fields] FROM \"users\" INNER JOIN \"posts\" ON \"users\".\"id\" = \"posts\".\"user_id\" INNER JOIN \"comments\" ON \"posts\".\"id\" = \"comments\".\"post_id\" INNER JOIN \"comments\" \"other_comments\" ON \"users\".\"id\" = \"other_comments\".\"author_id\"")
~~~

Results should also be correct: 

~~~ruby
  row = query.first
  row.other_comment.assert == row.comment
~~~


# Joining using `#connect`

You can also store joins in the entity definition (see `#belongs_to` and `#has_many` on the Entity) and then navigate through your models using those names.

A simple example for a connect: 

~~~ruby
  # Query for users, joining posts and joining comments from posts (anchor)
  query = users.connect(:posts).where(id: 1)

  users.
    join(:posts).on(:id => :author_id).
    where(id: 1).to_sql.assert == query.to_sql
~~~

Or a more complicated example, using hashes and arrays in the join specification.

~~~ruby
  # Query for users, joining posts and joining comments from posts (anchor)
  query = users.
    connect(:posts => [:comments, :approver]).
    where(:approver, id: 1)

  # This manual route is of course always available.
  approver = users.as(:approver, :approver)
  users.
    join(:posts).on(:id => :author_id).anchor.
    join(:comments).on(:id => :post_id).
    join(approver).on(:author_id => :id).
    where(approver, id: 1).
    to_sql.assert == query.to_sql
~~~

