# Transactions

Transactions are supported on the domain and on entities. Since you need to make sure that your transaction uses only one connection, you will need to handle connections explicitly. 

~~~ruby
  expect RuntimeError do
    domain.transaction do |conn| 
      posts.update(title: 'A single title for everyone').kick(conn)
      fail 'rollback'
    end
  end

  posts.first.title.assert != 'A single title for everyone'
~~~

The same works on any entity from the domain. 

~~~ruby
  posts.transaction do |conn|
    posts.all(conn)
  end
~~~

Domain transactions can be nested. Internally, this is realized using PostgreSQL savepoints.

~~~ruby
  domain.transaction do |connection|
    begin
      domain.transaction do |conn| 
        posts.update(title: 'A single title for everyone').kick(conn)
        fail 'rollback'
      end
    rescue RuntimeError
    end

    posts.first.title.assert != 'A single title for everyone'
  end
~~~

# Schemas & Search-Path

When connecting to a database, you can use the `search_path` argument to install a search path for all your accesses. 

~~~ruby
  pool = Flounder.connect(dbname: 'flounder', search_path: 'public,test')
  pool.with_connection do |connection|
    result = connection.exec 'show search_path'
    path = result.to_a.first.first.last
    path.assert == 'public, test'
  end
~~~

# Benchmarking

The time taken for each query is measured and aggregated. If you want to get data about how many queries you've executed and what time it took to execute them, please look at the following methods. 

~~~ruby
  stats = domain.stats

  # Please see https://github.com/josephruscio/aggregate for more documentation.
  stats.count.assert > 0

  # Can be printed to look nice
  stats.count
  stats.mean # kept in miliseconds
  stats.to_s # binary histogram!
~~~

Statistics are per thread.

~~~ruby
  # stats are per thread: 
  t = Thread.start do
    Thread.current.abort_on_exception = true
    domain.stats.count.assert == 0
    domain.stats.refute == stats
  end
  t.join
~~~

They can be reset, again per thread.

~~~ruby
  domain.reset_stats
  domain.stats.count.assert == 0
~~~