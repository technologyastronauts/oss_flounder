-- Database fixture for these tests. Please import into a database that 
-- should also be called 'flounder'.

CREATE EXTENSION IF NOT EXISTS hstore; 

DROP TABLE IF EXISTS "users" CASCADE;
CREATE TABLE "users" (
  "id" serial PRIMARY KEY,
  "name" varchar(40) NOT NULL, 
  "attributes" hstore NULL
);

BEGIN;
INSERT INTO "users" (name) VALUES ('John Snow');
INSERT INTO "users" (name) VALUES ('John Doe');
COMMIT;

DROP TABLE IF EXISTS "posts" CASCADE;
CREATE TABLE "posts" (
  "id" serial PRIMARY KEY,
  "title" varchar(100) NOT NULL,
  "text" text NOT NULL,
  "user_id" int NOT NULL REFERENCES users("id"), 
  "approver_id" int REFERENCES users("id")
);

BEGIN;
INSERT INTO "posts" (title, text, user_id, approver_id) VALUES (
  'First Light', 'This is the first post in our test system.', 1, 2);
COMMIT;

DROP TABLE IF EXISTS "comments" CASCADE;
CREATE TABLE "comments" (
  "id" serial PRIMARY KEY, 
  "post_id" int NOT NULL REFERENCES posts("id"),
  "text" text NOT NULL, 
  "author_id" int not null REFERENCES users("id")
);

BEGIN; 
INSERT INTO "comments" (post_id, text, author_id) VALUES (1, 'A silly comment.', 1); 
COMMIT; 

BEGIN; 
-- A table with structure to do performance measurements.
DROP TABLE IF EXISTS "perf01" CASCADE;
CREATE TABLE "perf01" (
  "id" serial not null primary key,
  "foo" int4,
  "bar" int4,
  "baz" int2 not null,
  "when" time(6),
  "count" int4,
  "iif" bool DEFAULT true,
  "type" varchar(255),
  "created_at" timestamp(6) not NULL default(now()),
  "updated_at" timestamp(6) not NULL default(now()),
  "bemerkung" text
);
COMMIT; 

-- import using
--
--    cat qed/flounder.sql | psql flounder
--
