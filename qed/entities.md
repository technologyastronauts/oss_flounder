

Flounder entities are your door to the database. They permit all sorts of meta-manipulation of database tables and fields. 

You can get an array of field values.

~~~ruby
    users.fields(:id, :name).assert == [:id, :name].map { |f| users[f] }    
~~~

You can get an array of all field values.

~~~ruby
    users.fields.assert == users.column_names.map { |f| users[f] }    
~~~

These very same field names print nicely for debugging.

~~~ruby
    users[:id].inspect.assert == 
        "<Flounder/Field <Flounder/Entity users(users)> id>"
~~~




