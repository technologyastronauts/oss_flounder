
THIS IS A TODO, NOT A FEATURE

# Atomic INSERT, then UPDATE

As an experimental feature, `Flounder` allows you to combine `INSERT` and `UPDATE` statements into one statement. This is executed as atomically as possible on the database. Two methods are responsible for this bit of magic, both composed of parts of the word 'INSERT' and 'UPDATE': `#insdate` and `#upsert`. We'll expose the difference between these two later on, for now, let's just jump right ahead and use them as intended.

For example, let's look at inserting a user only if it doesn't exist on the database yet.

~~~ruby
#  pre = users.count
#
#  users.insdate(:name, name: 'upsert')
#  users.insdate(:name, name: 'upsert')
#  
#  post = users.count
#  post.assert == pre+1
~~~

# Intricacies of design

Here's a decomposition of what the insdate is behind the scenes, into its parts: It serves as an illustration for turtles all the way down:
~~~ruby
  update = users.update(name: 'FooBar').where(name: 'BarBaz')

  users.insert(name: 'FooBar').
    with(:upsert, update).
    where(:id.not_in => 'upsert.id').
    assert generates_sql(
      %Q(WITH upsert AS ()))
~~~

Note that the above code does strictly nothing, since Arel (our SQL generator) currently does not support `WITH` for `INSERT`/`UPDATE`. For this reason, we decide not to implement upsert/insdate at this point. 