A simple use case. 

~~~ruby
  s2013 = domain[:users].where(:id => 1).first
  s2013.user.id.assert == 1
~~~

# Complex Conditions

Complex conditions can be written in SQL directly. You can use the postgres positional bind syntax to bind values to your conditions safely.

~~~ruby
  query = domain[:posts].where(["id = ($1) OR title = ($2)", 1, "Hello"])
  query.assert generates_sql(
      %Q(SELECT [fields] FROM "posts"  WHERE id = ($1) OR title = ($2)))

  post = query.first
  post.id.assert == 1
~~~