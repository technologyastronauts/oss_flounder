An update creates a state from which SQL can be extracted.

~~~ruby
  post = posts.first
  
  sql = posts.update(:title => 'Update SQL').where(:id => post.id).to_sql

  sql.assert == 'UPDATE "posts" SET "title" = $1 WHERE "posts"."id" = 1 RETURNING *'
~~~

Flounder fields can be used to identify the values to be set. 

~~~ruby
  post = posts.first
  
  sql = posts.
    update(posts[:title] => 'Update Flounder SQL').
    where(:id => post.id).to_sql

  sql.assert == 'UPDATE "posts" SET "title" = $1 WHERE "posts"."id" = 1 RETURNING *'
~~~

Using `where`, you can limit the records that are updated. 

~~~ruby
  post_id = posts.first.id
  post = posts.
    update(title: 'Update Field').
    where(id: post_id).kick.first

  post.title.assert == 'Update Field'
~~~

An update can take multiple fields.

~~~ruby
  post_id = 1
  
  statement = posts.update(
      title: 'Update SQL', 
      text: 'Update Multiple Fields Text', 
      approver_id: nil).
    where(:id => post_id)

  statement.to_sql.assert == %Q(UPDATE "posts" SET "title" = $1, "text" = $2, "approver_id" = $3 WHERE "posts"."id" = 1 RETURNING *)

  statement.kick
~~~

Updating a single row is possible.

~~~ruby
  post = posts.first
  
  post = posts.update(:title => 'Updated Title', :text => 'Update Single Row Possible').where(:id => post.id).kick.first

  post.title.assert == 'Updated Title'
  post.text.assert == 'Update Single Row Possible'
~~~

Updating multiple rows is possible.

~~~ruby
  updated = users.update(:name => 'Update Multiple Rows').where(:name.not_eq => nil).kick

  updated.map(&:name).assert == ['Update Multiple Rows']*7
~~~

Also, updating by using a subquery is possible. 

~~~ruby
  first_author = users.project(:id).limit(1)
  users.update(approver_id: first_author).where(:approver_id => nil).
    assert generates_sql("UPDATE \"users\" SET \"approver_id\" = (SELECT  \"users\".\"id\" FROM \"users\"  LIMIT 1) WHERE \"users\".\"approver_id\" IS NULL RETURNING *")
~~~
