
# Flounder.expr

Flounder also helps with the writing of complex SQL expressions. 

~~~ruby
  users = domain[:users]
  domain.expr { a(b(c(1, ' ', users[:id]))) }.cast('int').as(:foo).
    assert generates_sql("a(b(c(1, ' ', \"users\".\"id\")))::int AS foo")
~~~

You can also pass a condition-like hash to expr directly, it will return a Ruby object that allows for chaining. 

~~~ruby
  (users.cond(:posts, a: 1) | users.cond(a: 2)).
    assert generates_sql("((\"posts\".\"a\" = 1) OR (\"users\".\"a\" = 2))")
~~~

These condition pieces can be used in a WHERE-clause directly. 

~~~ruby
  users.where(users.cond(id: 1)).
   assert generates_sql("SELECT [fields] FROM \"users\"  WHERE (\"users\".\"id\" = 1)") 
~~~
