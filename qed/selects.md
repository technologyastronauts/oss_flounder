

A simple domain definition.

~~~ruby  
  # See 'setup_domain.rb' for the domain definition. Repeated here, as a 
  # comment: 
  #
  #   Flounder.domain(connection) do |dom|
  #     dom.entity(:users, :user, 'users')
  #     dom.entity(:posts, :post, 'posts')
  #     dom.entity(:comments, :comment, 'comments')
  #   end

  # Enable this line if you want to see all statements executed.
  # domain.logger = Logger.new(STDOUT)
~~~

And a simple use case. 

~~~ruby
  s2013 = domain[:users].where(:id => 1).first
  s2013.user.id.assert == 1
~~~

If we want to see all records, we use the `all` kicker, which has some synonyms. You best discover those by treating the domain entity as an array.  

~~~ruby
  users = domain[:users].all
  users.size.assert == 7
  users.assert.kind_of? Array

  domain[:users].map(&:id).assert == users.map(&:id)
~~~

# Treat it like an array

You can treat the entities and the queries like an array. 

~~~ruby
  entity = domain[:users]
  entity.size.assert > 0

  query = entity.where(:id => :id)
  query.size.assert == entity.size
~~~

# `LIMIT` and `OFFSET`

Limit the amount of results fetched: 

~~~ruby
  rows = domain[:users].limit(1).order_by(:id).to_a
  rows.size.assert == 1

  rows = domain[:users].order_by(:id).limit(1).to_a
  rows.size.assert == 1
~~~

And start at a given offset: 

~~~ruby
  rows = domain[:users].order_by(:id).offset(1).to_a
  rows.first.id.assert == 2
~~~

As a special case, calling `#first` on a query will `.limit(1).first`. 

# Subqueries

Sometimes you have to say it with a subquery. For example, you might want to look at the latest post for each author in the database. 

~~~ruby
  last_post = posts.
    project(:id).
    order_by(:id.desc). # for lack of timestamps, not as a best practice
    where(:approver_id => domain[:users][:id]).limit(1)

  query = domain[:users].
    join(posts).on(posts[:id] => last_post)

  query.assert generates_sql(%Q(SELECT [fields] FROM "users" INNER JOIN "posts" ON "posts"."id" = (SELECT  "posts"."id" FROM "posts"  WHERE "posts"."approver_id" = "users"."id"  ORDER BY "posts"."id" DESC LIMIT 1)))

  row = query.first
  row.post.id.assert == 1
~~~

# GROUP BY and HAVING

~~~ruby
  posts.group_by(:id).having(id: 0, user_id: 2).project('id').
    assert generates_sql("SELECT [fields] FROM \"posts\"  GROUP BY \"posts\".\"id\" HAVING \"posts\".\"id\" = 0 AND \"posts\".\"user_id\" = 2")
  
  posts.group_by(:id).having('id = 1').project('id').
    assert generates_sql("SELECT [fields] FROM \"posts\"  GROUP BY \"posts\".\"id\" HAVING id = 1")
~~~