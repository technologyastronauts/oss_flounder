

def domain
  @domain ||= begin
    connection = Flounder.connect(dbname: 'flounder')
    Flounder.domain(connection) do |dom|
      dom.entity(:users, :user, 'users') do |user|
        user.has_many :posts, :id => :author_id
        user.has_many :approved_posts, :posts, :id => :author_id
        user.has_many :comments, :id => :author_id
      end
      dom.entity(:posts, :post, 'posts') do |post|
        post.belongs_to :user, :author_id => :id
        post.belongs_to :approver, :users, :author_id => :id

        post.has_many :comments, :id => :post_id
      end
      dom.entity(:comments, :comment, 'comments') do |comment|
        comment.belongs_to :post, :post_id => :id
        comment.belongs_to :author, :author_id => :id
      end
    end
  end
end

# Allows using entities directly, without indirection via domain. (ie `users`)
domain.entities.each do |entity|
  define_method entity.plural do
    entity
  end
end

# Load the SQL fixtures.
#
# One fine day we will use transactional features.
#
require 'pathname'
sql = Pathname.new(__FILE__).dirname.join('../..').join('qed/flounder.sql')
File.open sql do |file|
  Flounder::Engine.new(domain.connection_pool).exec file.read
end