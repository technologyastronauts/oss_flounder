require 'ae'

def generates_sql(expected)
  -> (given) {
    given.to_sql.gsub(/^SELECT.*?FROM/, 'SELECT [fields] FROM').assert == expected
  }
end