Results can be ordered.

~~~ruby
  sql = domain[:posts].order_by(:title).to_sql

  sql.assert == 'SELECT "posts"."id" AS _posts_id, "posts"."title" AS _posts_title, "posts"."text" AS _posts_text, "posts"."user_id" AS _posts_user_id, "posts"."approver_id" AS _posts_approver_id FROM "posts"   ORDER BY "posts"."title"'
~~~

They can be ordered ASC.

~~~ruby
  sql = domain[:posts].order_by(:title.asc).to_sql

  sql.assert == 'SELECT "posts"."id" AS _posts_id, "posts"."title" AS _posts_title, "posts"."text" AS _posts_text, "posts"."user_id" AS _posts_user_id, "posts"."approver_id" AS _posts_approver_id FROM "posts"   ORDER BY "posts"."title" ASC'
~~~

They can also be ordered DESC.

~~~ruby
  domain[:posts].order_by(:title.desc).
    assert generates_sql(
      'SELECT [fields] FROM "posts"   ORDER BY "posts"."title" DESC')
~~~

Multiple fields can be used.

~~~ruby
  sql = domain[:posts].order_by(:title, :text).to_sql

  sql.assert == 'SELECT "posts"."id" AS _posts_id, "posts"."title" AS _posts_title, "posts"."text" AS _posts_text, "posts"."user_id" AS _posts_user_id, "posts"."approver_id" AS _posts_approver_id FROM "posts"   ORDER BY "posts"."title", "posts"."text"'
~~~

Multiple fields can be used with ASC, DESC.

~~~ruby
  sql = domain[:posts].order_by(:title.asc, :text.desc).to_sql

  sql.assert == 'SELECT "posts"."id" AS _posts_id, "posts"."title" AS _posts_title, "posts"."text" AS _posts_text, "posts"."user_id" AS _posts_user_id, "posts"."approver_id" AS _posts_approver_id FROM "posts"   ORDER BY "posts"."title" ASC, "posts"."text" DESC'
~~~

Ordering can be defined in many ways.

~~~ruby
  user = users.first

  inserted = posts.insert(
    title: 'ABC', text: 'Alphabet', user_id: user.id).kick

  def titles_by *args
    domain[:posts].order_by(*args).map(&:title)
  end

  titles_by(:title).assert == ['ABC', 'First Light']
  titles_by(:title.asc).assert == ['ABC', 'First Light']
  titles_by(:title.desc).assert == ['First Light', 'ABC']

  titles_by('title').assert == ['ABC', 'First Light']
  titles_by('title ASC').assert == ['ABC', 'First Light']
  titles_by('title DESC').assert == ['First Light', 'ABC']

  titles_by(posts[:title]).assert == ['ABC', 'First Light']
  titles_by(posts[:title].asc).assert == ['ABC', 'First Light']
  titles_by(posts[:title].desc).assert == ['First Light', 'ABC']
~~~

A call to order by replaces an earlier call.

~~~ruby
  query = domain[:posts].order_by(:title.asc)
  sql = query.to_sql

  sql.assert == 'SELECT "posts"."id" AS _posts_id, "posts"."title" AS _posts_title, "posts"."text" AS _posts_text, "posts"."user_id" AS _posts_user_id, "posts"."approver_id" AS _posts_approver_id FROM "posts"   ORDER BY "posts"."title" ASC'
  
  sql = query.order_by(:title.asc, :text.desc).to_sql

  sql.assert == 'SELECT "posts"."id" AS _posts_id, "posts"."title" AS _posts_title, "posts"."text" AS _posts_text, "posts"."user_id" AS _posts_user_id, "posts"."approver_id" AS _posts_approver_id FROM "posts"   ORDER BY "posts"."title" ASC, "posts"."text" DESC'
~~~