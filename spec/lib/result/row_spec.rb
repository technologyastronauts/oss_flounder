
# Run tests using
#   ruby -Ilib:test TEST_FILE

require 'spec_helper'

describe Flounder::Result::Row do
  class DescriptorStub
    def initialize hash
      @hash = hash
    end

    def has_obj? name
      @hash.has_key?(name)
    end

    def [] name
      val = @hash.fetch(name)

      if val.kind_of? Hash
      else
        ValueStub.new(val)
      end
    end

    def names
      @hash.keys
    end
  end
  class ValueStub
    def initialize value
      @value = value
    end
    def produce_value *a
      @value
    end
  end

  let(:descriptor) { DescriptorStub.new(:foo => :bar, :bar => :baz) }
  let(:row) { Flounder::Result::Row.new(descriptor, 1) }

  it 'allows field access through methods' do
    row.foo.assert == :bar
  end
  it 'raises when accessing fields that don\'t exist' do
    NoMethodError.raised? do
      row.bar
    end
  end
  it 'has a useful inspect' do
    row.inspect.assert == "flounder/Row([:foo, :bar])"
  end

  describe 'hash-like features' do
    it 'has [] accessor' do
      row[:foo].assert == :bar
    end
    it 'has __columns__ list' do
      row.__columns__.assert == [:foo, :bar]
    end

    it 'converts to a hash (#to_h)' do
      row.to_h.assert == {:foo => :bar, :bar => :baz}
    end

    it 'has #values_at accessor' do
      row.values_at(:foo, :bar).assert == [:bar, :baz]
    end
  end
end