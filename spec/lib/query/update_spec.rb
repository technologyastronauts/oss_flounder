
require 'spec_helper'

describe Flounder::Query::Update do 
  define_domain

  let(:update) { domain[:users].update }

  it 'returns an update from #set' do
    update.set({}).assert.kind_of?(described_class)
  end
end