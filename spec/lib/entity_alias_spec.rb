

require 'spec_helper'

describe Flounder::EntityAlias do
  class Table
    def alias *a
      self
    end

    def [] name
      "field #{name}"
    end
  end

  let(:table) { Table.new }
  let(:entity) { flexmock('entity', 
    table: table, name: 'entity', table_name: 'table') }

  let(:ea) { Flounder::EntityAlias.new(entity, :aliases, :alias) }

  describe '#fields' do
    it 'returns a list of fields' do
      ea.fields(:a, :b).assert == [ea[:a], ea[:b]]
    end
  end
end