

require 'rspec'
require 'flexmock'
require 'ae/adapters/rspec'

RSpec.configure do |config|
  config.mock_with :flexmock
end

require 'flounder'

def define_domain
  define_method(:domain) do
    @domain ||= begin
      connection = Flounder.connect(dbname: 'flounder')
      Flounder.domain(connection) do |dom|
        dom.entity(:users, :user, 'users') do |user|
          user.has_many :posts, :id => :author_id
          user.has_many :approved_posts, :posts, :id => :author_id
          user.has_many :comments, :id => :author_id
        end
        dom.entity(:posts, :post, 'posts') do |post|
          post.belongs_to :user, :author_id => :id
          post.belongs_to :approver, :users, :author_id => :id

          post.has_many :comments, :id => :post_id
        end
        dom.entity(:comments, :comment, 'comments') do |comment|
          comment.belongs_to :post, :post_id => :id
          comment.belongs_to :author, :author_id => :id
        end
      end
    end
  end
end