require 'pg'
require 'arel'

require 'flounder/symbol_extensions'

require 'flounder/postgres_utils'
require 'flounder/connection'
require 'flounder/connection_pool'
require 'flounder/domain'
require 'flounder/engine'
require 'flounder/entity'
require 'flounder/entity_alias'
require 'flounder/field'
require 'flounder/immediate'

require 'flounder/query/select'
require 'flounder/query/insert'
require 'flounder/query/update'
require 'flounder/query/delete'

require 'flounder/exceptions'

require 'flounder/expression'

require 'flounder/result/descriptor'
require 'flounder/result/row'

module Flounder
module_function
  def connect opts={}
    ConnectionPool.new(opts)
  end

  def domain connection, &block
    Domain.new(connection).tap { |d| yield d if block_given? }
  end

  def literal str
    Arel::Nodes::SqlLiteral.new(str)
  end
  module_function :literal
end

Symbol.send(:include, Flounder::SymbolExtensions)