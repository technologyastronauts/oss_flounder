require 'connection_pool'

module Flounder
  class ConnectionPool
    attr_reader :pg_conn_args

    def initialize pg_conn_args
      @pg_conn_args = pg_conn_args
      @pool = ::ConnectionPool.new(size: 5, timeout: 5) {
        Connection.new(pg_conn_args)
      }
    end

    # Checks out a connection from the pool and yields it to the block. The
    # connection is returned to the pool at the end of the block; don't hold
    # on to it. 
    #
    def with_connection
      @pool.with do |conn|
        yield conn
      end
    end

    # Checks out a connection from the pool. You have to return this
    # connection manually.
    #
    def checkout
      @pool.checkout
    end

    Spec = Struct.new(:config)

    # This is needed to conform to arels interface. 
    #
    def spec
      Spec.new(adapter: 'pg')
    end
  end
end