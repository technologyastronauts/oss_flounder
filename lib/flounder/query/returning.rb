

module Flounder::Query
  module Returning
    def returning_fields
      @returning_fields || '*'
    end

    def returning fields
      @returning_fields = fields

      self
    end

    def to_sql
      super << " RETURNING #{returning_fields}"
    end
  end
end