require_relative 'base'
require_relative 'returning'

module Flounder::Query

  # An update obtained by calling any of the chain methods on an entity.
  #
  class Update < Base
    def initialize domain, entity
      super(domain, Arel::UpdateManager, entity)

      manager.table entity.table
    end

    # Add one row to the updates.
    #
    def set fields
      manager.set(
        fields.map { |k, v| 
          transform_tuple_for_set(k, v) })

      self
    end

    include Returning

  end # class
end # module Flounder