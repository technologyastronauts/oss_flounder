require_relative 'base'

module Flounder::Query

  # An update obtained by calling any of the chain methods on an entity.
  #
  class Delete < Base
    def initialize domain, entity
      super(domain, Arel::DeleteManager, entity)

      manager.from entity.table
    end
  end # class
end # module Flounder