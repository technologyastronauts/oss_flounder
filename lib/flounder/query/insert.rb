 
require_relative 'base'
require_relative 'returning'

module Flounder::Query

  # An insert.
  #
  class Insert < Base
    def initialize domain, into_entity
      super domain, Arel::InsertManager, into_entity

      manager.into entity.table
    end

    # Add one row to the inserts.
    #
    def row fields
      manager.insert(
        fields.map { |k, v| 
          transform_tuple_for_set(k, v) })
    end

    include Returning
  end # class
end # module Flounder