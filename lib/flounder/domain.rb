
require 'forwardable'
require 'logger'
require 'aggregate'

module Flounder
  class Domain
    # A device that discards all logging made to it. This is used to be silent
    # by default while still allowing the logging of all queries. 
    #
    class NilDevice
      def write *args
      end
      def close *args
      end
    end
    
    def initialize connection_pool
      @connection_pool = connection_pool

      # maps from plural/singular names to entities in this domain
      @plural = {}
      @singular = {}

      # Maps table names to entities
      @entity_by_table_name = {}

      # maps OIDs of entities to entities
      @oids_entity_map = Hash.new { |hash, oid|
        hash[oid] = entity_from_oid(oid)
      }

      @logger = Logger.new(NilDevice.new)

      @savepoints = Hash.new(0)
    end

    attr_reader :savepoints
    attr_reader :connection_pool
    attr_accessor :logger

    extend Forwardable
    def_delegators :connection_pool, :with_connection

    def transaction &block
      with_connection do |conn|
        if in_transaction?(conn)
          savepoint(conn) { block.call(conn) }
        else
          conn.transaction do
            savepoint(conn) { block.call(conn) }
          end
        end
      end
    end

    # Builds an SQL expression. 
    # 
    #   domain.expr { concat('1', '2') }
    #
    def expr &block
      builder = Expression::Builder.new(self)
      builder.call(&block)
    end

    # Returns an aggregate of all query wall clock times. Please see 
    # https://github.com/josephruscio/aggregate for more information on this 
    # object. 
    #
    # @return [Aggregate] statistics for this thread
    # 
    def stats
      Thread.current[:__flounder_stats] ||= Aggregate.new
    end

    # Resets this threads stats. 
    #
    def reset_stats
      Thread.current[:__flounder_stats] = nil
    end

    # Returns the entity with the given plural name. 
    #
    def [] name
      raise NoSuchEntity, "No such entity #{name.inspect} in this domain." \
        unless @plural.has_key?(name)

      @plural.fetch(name)
    end

    # Returns all entities as an array. 
    #
    def entities
      @plural.values
    end
    def has_entity? name
      @plural.has_key? name
    end

    # Logs sql statements that are prepared for execution. 
    #
    def log_sql sql
      @logger.info sql
    end
    def log_bm measure
      stats << measure.total * 1_000
      @logger.info "Query took #{measure}."
    end

    # Define a database entity and alias it to plural and singular names that 
    # will be used in the code. 
    #
    def entity plural, singular, table_name
      entity = Entity.new(self, plural, singular, table_name).
        tap { |e| yield e if block_given? }

      raise ArgumentError, "Table #{table_name} was already associated with an entity." \
        if @entity_by_table_name.has_key?(table_name)

      @plural[plural] = entity
      @singular[singular] = entity
      @entity_by_table_name[table_name] = entity

      entity
    end

    # Returns an entity by table oid.
    #
    def by_oid oid
      return nil if oid==0 # computed fields
      @oids_entity_map[oid]
    end

  private
    def sp_count connection
      savepoints[connection.object_id]
    end
    def inc_sp connection
      savepoints[connection.object_id] += 1
    end
    def dec_sp connection
      savepoints[connection.object_id] -= 1
      if savepoints[connection.object_id] < 0
        fail "ASSERTION FAILURE: Savepoint count cannot drop below zero!"
      end

      nil
    end
    def in_transaction? connection
      sp_count(connection) > 0
    end
    def savepoint connection, &block
      sp_number = inc_sp(connection)
      sp_name = "s#{sp_number}"

      connection.exec("SAVEPOINT #{sp_name};")
      begin
        yield
      rescue Exception
        connection.exec("ROLLBACK TO SAVEPOINT #{sp_name};")
        raise
      ensure
        dec_sp(connection)
      end
    end

    def entity_from_oid oid
      connection_pool.with_connection do |conn|
        table_name = conn.exec(%Q(SELECT #{oid}::regclass)).
          getvalue(0,0)

        @entity_by_table_name[table_name]
      end
    end
  end
end