

module Flounder
  # Raised when an entity name cannot be found in this domain. 
  class NoSuchEntity < StandardError; end
  
  # Indicates a field reference in a chain method that cannot be resolved 
  # to a real field on the underlying relation. 
  #
  class InvalidFieldReference < StandardError; end

  # Indicates that a where clause with bind variables went out of bounds. 
  class BindIndexOutOfBounds < StandardError; end

  # Exception raised whenever a result set contains two columns with the 
  # same name. 
  #
  class DuplicateField < StandardError; end
end