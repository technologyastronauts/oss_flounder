require 'flounder/helpers/entity'

module Flounder
  class Relation
    attr_accessor :domain, :type
    attr_accessor :name, :entity_ref, :join_conditions

    def initialize domain, type, name, entity_ref, join_conditions
      @domain, @type, @name, @entity_ref, @join_conditions = 
        domain, type, name, entity_ref, join_conditions
    end

    def linked_entity
      @linked_entity ||= begin
        entity = convert_to_entity(entity_ref)

        if name != entity_ref
          # TODO maybe using name for both singular and plural is not a good
          # idea. Maybe it is.
          entity = entity.as(name, name)
        end

        entity
      end
    end

    def apply query
      query.
        join(linked_entity).
        on(join_conditions)
    end

  private
    # Checks if a given symbol can be an entity after resolution. 
    #
    def has_entity? sym
      domain.has_entity?(sym)
    end

    # Resolves an entity through the domain. 
    #
    def resolve_entity sym
      domain[sym]
    end

    # Now that the above two methods exist, we can use this helper:
    include Flounder::Helpers::Entity
  end
end