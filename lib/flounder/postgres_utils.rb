
require 'time'
require 'date'
require 'pg_hstore'

module Flounder
  module PostgresUtils
    OID_BOOLEAN = 16
    OID_BYTEA = 17
    OID_SMALLINT = 21
    OID_INTEGER = 23
    OID_TEXT = 25
    OID_FLOAT = 701
    OID_VARCHAR = 1043
    OID_TIMESTAMP = 1114
    OID_DATE = 1082
    OID_TIME = 1083
    OID_TIMESTAMPTZ = 1184

    def typecast connection, type_oid, value
      raise ArgumentError, "AF: type_oid must not be nil" unless type_oid

      return nil unless value

      # hstore extension
      if type_oid == oid_hstore(connection)
        return PgHstore.load(value) 
      end

      # assert: value is not nil
      case type_oid
        when OID_TIMESTAMPTZ
          DateTime.parse(value)
        when OID_TIMESTAMP
          DateTime.parse(value)
        when OID_DATE
          Date.parse(value)
        when OID_TIME
          Time.parse(value)
        when OID_INTEGER, OID_SMALLINT
          value.to_i
        when OID_FLOAT
          value.to_f
        when OID_BOOLEAN
          value == 't'
        when OID_TEXT
          value.to_s      
      else
        value
      end
    end

    def oid_hstore connection
      unless @oid_hstore_initialized
        @oid_hstore_initialized = true

        @oid_hstore = begin 
          result = connection.exec("SELECT oid FROM pg_type WHERE typname='hstore'")
          row = result.first

          row && row.values.first.to_i
        end
      end

      @oid_hstore
    end

    def type_oid_to_sym connection, oid
      raise ArgumentError, "AF: oid must not be nil" unless oid

      return :hash if oid==oid_hstore(connection)

      case oid
        when OID_BYTEA
          :binary
        when OID_TIMESTAMP, OID_TIMESTAMPTZ
          :datetime
        when OID_DATE
          :date
        when OID_TIME
          :time
        when OID_INTEGER, OID_SMALLINT
          :integer
        when OID_FLOAT
          :float
        when OID_VARCHAR, OID_TEXT
          :string
        when OID_BOOLEAN
          :boolean
      else
        nil
      end
    end
    
    # Yields header information for each column in the given result in turn. 
    # 
    # @yieldparam idx [Integer] column index
    # @yieldparam entity [Flounder::Entity] entity as resolved by table OID
    # @yieldparam field_name [String] field name as present in the SQL result
    # @yieldparam type [Integer] type OID
    # @yieldparam binary [Boolean] is this a binary field?
    #
    def each_field entity, result
      domain = entity.domain

      result.nfields.times do |field_idx|
        table_oid = result.ftable(field_idx)
        entity = domain.by_oid(table_oid)

        yield(
          field_idx, 
          entity,
          result.fname(field_idx),
          result.ftype(field_idx), 
          result.fformat(field_idx) == 1)
      end
    end

    def access_value connection, result, type_oid, row_idx, col_idx
      value = result.getvalue(row_idx, col_idx)
      typecast(connection, type_oid, value)
    end

    # Helper function for debugging
    def type_name ftype, fmod
      pg.
        exec( "SELECT format_type($1,$2)", 
          [ftype, fmod] ).
        getvalue( 0, 0 )
    end
  end
end