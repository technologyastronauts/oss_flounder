module Flounder::Helpers

  # Helper module that helps with entity resolution. To include this module in
  # a class, you need to provide the following two methods: 
  # 
  #   * has_entity?(sym) -> Boolean
  #     Decides if a given symbol can be converted into an entity
  #   * resolve_entity(sym) -> Entity
  #     Resolves an entity from a symbol name.
  #
  module Entity
    def entity_like? something
      something.kind_of?(Flounder::Entity) ||
        something.kind_of?(Flounder::EntityAlias) ||
        something.kind_of?(Symbol) && has_entity?(something)
    end
    def convert_to_entity something
      case something
        when Flounder::Entity, Flounder::EntityAlias
          return something
        when Symbol
          return resolve_entity(something)
      else
        fail "Not entity-like - yet! (#{something.inspect})"
      end
    end
  end
end