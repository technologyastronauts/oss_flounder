
module Flounder::Expression
  class Expr
    def initialize domain
      @domain = domain
    end

    def as name
      Named.new(@domain, name, self)
    end
    def cast type
      Cast.new(@domain, type, self)
    end
    def | expr
      BinaryOp.new(@domain, 'OR', self, expr)
    end
    def & expr
      BinaryOp.new(@domain, 'AND', self, expr)
    end

    def eval argument
      case argument
        when String, Symbol
          db_quote(argument.to_s)
        when Fixnum
          argument
        when Expr
          argument.to_sql
        when Flounder::Field
          argument.fully_qualified_name
      else 
        db_quote(argument)
      end
    end

    def db_quote something
      @domain.with_connection do |conn|
        conn.quote(something)
      end
    end

    def to_sql
      raise NotImplementedError
    end
    def to_immediate
      Flounder::Immediate.new(to_sql)
    end

    include Flounder::SymbolExtensions
  end

  class Cast < Expr
    def initialize domain, type, expr
      super(domain)

      @type = type
      @expr = expr
    end

    def to_sql
      @expr.to_sql << '::' << @type.to_s
    end
  end

  class Named < Expr
    def initialize domain, name, expr
      super(domain)

      @name = name
      @expr = expr
    end

    def to_sql
      @expr.to_sql << ' AS ' << @name.to_s
    end
  end

  class FunCall < Expr
    def initialize domain, name, arguments
      super(domain)

      @name = name
      @arguments = arguments
    end

    def to_sql
      @name.to_s << '(' << 
        @arguments.map { |arg| eval(arg) }.join(', ') << ')'          
    end
  end

  class ConditionBit < Expr
    def initialize domain, engine, bit
      super(domain)
      @engine = engine
      @bit = bit
    end

    attr_accessor :bit, :engine

    def to_sql
      bit.to_sql(engine)
    end
  end

  class BinaryOp < Expr
    def initialize domain, op_string, *terms
      super(domain)

      @op_string = op_string
      @terms = terms
    end

    attr_accessor :op_string

    def concat term
      @terms << term
    end

    def to_sql
      "(" +
        @terms.
          map { |t| eval(t) }.
          join(" #{op_string} ") +
        ")"
    end
  end

  class Builder
    def initialize domain
      @domain = domain
    end

    attr_accessor :domain

    def call &block
      instance_eval(&block)
    end

    # Conditions are interpreted relative to an entity. 
    def interpret_conditions entity, conditions
      # The method we need is in there...
      query = Flounder::Query::Select.new(domain, entity)
      engine = Flounder::Engine.new(domain.connection_pool)

      and_expr = BinaryOp.new(domain, 'AND')

      # TODO parse_conditions and its call tree is not really something that
      # belongs into the query object - we should create a new abstraction here.
      query.parse_conditions(*conditions) { |bit| 
        and_expr.concat(ConditionBit.new(domain, engine, bit)) }

      and_expr
    end

    def respond_to? sym, include_all=false
      true
    end
    def method_missing sym, *args, &block
      raise ArgumentError, "No blocks in sql execution context." if block

      FunCall.new(domain, sym, args)
    end
  end
end