module Flounder
  module SymbolExtensions
    class Modifier < Struct.new(:sym, :kind)
      def to_arel_field entity
        af = case sym
          when Symbol
            entity[sym].arel_field
          when Flounder::Field
            sym.arel_field
          when Flounder::Expression::Expr
            sym.to_immediate.to_arel_field
        else
          fail "ASSERTION FAILURE: Unknown type in field.sym: #{field.sym.inspect}."
        end
      end
    end

    # NOTE mixing comparison ops with asc and desc is nasty, but not really 
    # relevant. Errors will get raised later on - we're not in the business of 
    # checking your SQL for validity. 
    [:not_eq, :lt, :gt, :gteq, :lteq, :matches, :in, :not_in, 
      :asc, :desc].each do |kind|
      define_method kind do
        Modifier.new(self, kind)
      end
    end
  end
end