module Flounder
  class Field
    def initialize entity, name, arel_field
      @entity = entity
      @name = name
      @arel_field = arel_field
    end

    # @return [Entity] entity this field belongs to
    attr_reader :entity

    # @return [Arel::Attribute] arel attribute that corresponds to this field
    attr_reader :arel_field

    # @return [String] name of this field
    attr_reader :name

    # Returns a fully qualified name (table.field).
    #
    # @return [String] fully qualified field name
    #
    def fully_qualified_name
      entity.with_connection do |conn|
        table = conn.quote_table_name(entity.table_name)
        column = conn.quote_column_name(name)
        "#{table}.#{column}"
      end
    end

    def to_arel_field
      arel_field
    end

    # Allow comparison and inclusion in hashes.
    def == other
      self.entity == other.entity && self.name == other.name
    end
    alias eql? ==
    def hash
      self.entity.hash ^ self.name.hash
    end

    # Allow printing for debug purposes
    def inspect
      "<Flounder/Field #{entity} #{name}>"
    end
    alias to_s inspect

    include SymbolExtensions
  end
end