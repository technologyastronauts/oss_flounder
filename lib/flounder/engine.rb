module Flounder

  # Intermediary class that arel wants us to create. Mostly supports the 
  # #connection_pool message returning our connection pool. 
  #
  class Engine
    attr_reader :connection_pool
    attr_reader :connection

    def initialize connection_pool
      @connection_pool = connection_pool
      # TBD This connection is currently never returned to the pool, Arel 
      # is designed that way. 
      @connection = connection_pool.checkout
    end

    def exec *args, &block
      connection_pool.with_connection do |conn|
        conn.exec *args, &block
      end
    end

    # ---------------------------------------------------- official Engine iface

    # Returns the connection pool. 
    # 
    # @return [ConnectionPool]
    #
    def connection_pool
      @connection_pool
    end
  end
end