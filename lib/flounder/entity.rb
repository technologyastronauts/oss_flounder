require 'forwardable'

require 'flounder/relation'

module Flounder

  # An entity corresponds to a table in the database. On top of its table 
  # name, an entity will know its code name (plural) and its singular name, 
  # what to call a single row returned from this entity. 
  #
  # An entity is not a model. In fact, it is what you need to completely
  # hand-roll your models, without being the model itself.
  #
  # Entities are mainly used to start a query via one of the query initiators. 
  # Almost all of the chain methods on the Query object can be used here as
  # well - they will return a query object. Entities, like queries, can be 
  # used as enumerables. 
  #
  # Example: 
  #
  #   foo = domain.entity(:plural, :singular, 'table_name')
  # 
  #   foo.all # SELECT * FROM table_name
  #   foo.where(id: 1).first  # SELECT * FROM table_name WHERE "table_name"."id" = 1
  #
  class Entity
    def initialize domain, plural, singular, table_name
      @domain = domain
      @name = plural
      @singular = singular
      @table_name = table_name
      @columns_hash = nil
      @relations = {} # name -> relation

      @table = Arel::Table.new(table_name)
    end

    # @return [Domain] Domain this entity is defined in.
    attr_reader :domain

    # @return [Symbol] Name of the entity in plural form. 
    attr_reader :name
    
    # Also, the name is the plural, so we'll support that as well. 
    alias plural name

    # @return [Symbol] Name of the entity in singular form. 
    attr_reader :singular

    # @return [Arel::Table] Arel table that underlies this entity. 
    attr_reader :table
    # @return [String] Name of the table that underlies this entity. 
    attr_reader :table_name

    # @return [Array] relations that this entity has to other entities
    attr_accessor :relations

    extend Forwardable
    def_delegators :domain, :transaction, :with_connection

    # @return [Field] Field with name of the entity.
    #
    def [] name
      Field.new(self, name, table[name])
    end
    def fields *names
      (names.empty? ? column_names : names).map { |name| self[name] }
    end

    def inspect
      "<Flounder/Entity #{name}(#{table_name})>"
    end
    alias to_s inspect

    # Relations to other entities
    
    # Adds a relationship where many records on the other entity correspond to
    # this one.
    #
    def has_many *a
      add_relationship :has_many, *a
    end

    # Adds a relationship where one record on the other entity corresponds to
    # possibly many on our side.
    #
    # Example: 
    #   domain.entity :foos, :foo, 'foo' do |foo|
    #     foo.belongs_to :bar, :bar_id => :id
    #   end
    #
    # You can also explicitly name the relationship instead of going through 
    # the singular name of the other entity: 
    #
    # Example: 
    #   domain.entity :foos, :foo, 'foo' do |foo|
    #     foo.belongs_to :bar_stuff, :bar, :bar_id => :id
    #   end
    #
    def belongs_to *a
      add_relationship :belongs_to, *a
    end

    def add_relationship type, name, entity_ref=nil, **join_conditions
      raise ArgumentError, "Must have join conditions." if join_conditions.empty?
      
      relations[name] = Relation.new(
        domain, type, name, entity_ref||name, join_conditions)
    end

    # Builds a condition part or a general SQL expression. 
    # 
    #   entity.cond(a: 1)
    #
    def cond *conditions
      builder = Expression::Builder.new(domain)
      builder.interpret_conditions(self, conditions)
    end

    # Starts a new select query and yields it to the block. Note that you don't 
    # need to call this method to obtain a select query - any of the methods
    # on Query::Select should work on the entity and return a new query object. 
    #
    # @return [Query::Select]
    #
    def select
      Query::Select.new(domain, self).tap { |q| 
        yield q if block_given? 
      }
    end
    
    def insert hash
      Query::Insert.new(domain, self).tap { |q| 
        q.row(hash) }
    end
    
    def update hash={}
      Query::Update.new(domain, self).tap { |u|
        u.set(hash) }
    end

    def delete 
      Query::Delete.new(domain, self)
    end
        
    # Temporarily creates a new entity that is available as if it was declared
    # with the given plural and singular, but referencing to the same
    # underlying relation.
    #
    def as plural, singular
      EntityAlias.new(self, plural, singular)
    end 

    def columns_hash
      @columns_hash ||= begin
        domain.connection_pool.with_connection do |conn|
          conn.columns_hash(table_name)
        end
      end      
    end
    def column_names
      columns_hash.keys
    end

    # Query initiators
    [:where, :join, :outer_join, :project,
      :order_by, :group_by, 
      :limit, :offset, :connect].each do |name|
      define_method name do |*args|
        select { |q| q.send(name, *args) }
      end
    end

    # Kickers
    [:first, :all, :size].each do |name|
      define_method name do |*args|
        q = select
        q.send(name, *args)
      end
    end

    def each &block
      all.each &block
    end
    include Enumerable
  end
end