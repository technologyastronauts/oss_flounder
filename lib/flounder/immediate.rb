
module Flounder
  # An immmediate string that needs to be passed around and _not_ quoted or
  # escaped when going to the database. 
  #
  # @private
  class Immediate
    def initialize string
      @string = string
    end

    attr_reader :string

    def to_arel_field
      Arel::SqlLiteral.new(string)
    end
  end
end