
module Flounder::Result::Accessor
  class Field
    attr_reader :descriptor
    attr_reader :col_idx
    attr_reader :type_oid

    def initialize descriptor, col_idx, type_oid
      @descriptor = descriptor
      @col_idx = col_idx
      @type_oid = type_oid
    end

    def produce_value(row_idx)
      descriptor.value(type_oid, row_idx, col_idx)
    end
  end
end