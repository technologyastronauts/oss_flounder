
require_relative 'field'

module Flounder::Result::Accessor
  class Node 
    attr_reader :children_by_name

    def initialize 
      @children_by_name = Hash.new do |hash, name|
        hash[name.to_sym] = Node.new
      end
    end

    def [] name
      children_by_name[name.to_sym]
    end
    def has_obj? name
      children_by_name.has_key? name.to_sym
    end
    def names
      children_by_name.keys
    end

    def add_field name, *a
      children_by_name[name.to_sym] = Field.new(*a)
    end

    def size
      children_by_name.size
    end

    def produce_value *_
      yield self
    end
  end
end