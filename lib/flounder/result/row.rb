module Flounder::Result
  class Row
    def initialize node, row_idx
      @root = node
      @row_idx = row_idx
      @attributes = {}
    end

    # Primary resolution mechanism: Lazy lookup of fields. 

    def method_missing sym, *args, &block
      if @root.has_obj?(sym)
        return cache_attribute(sym)
      end

      if sym.to_s.end_with?(??)
        stripped = sym.to_s[0..-2]
        return @root.has_obj?(stripped) && !value_for(stripped).nil?
      end

      super
    end
    def respond_to? sym, include_all=false
      @attributes.has_key?(sym) ||
        @root.has_obj?(sym) || 
        super
    end
    def methods regular=true
      (__columns__ + super).uniq
    end

    def inspect
      "flounder/Row(#{__columns__.inspect})"
    end

    def == other
      if other.kind_of?(Row)
        other_root = other.instance_variable_get('@root')

        __columns__ == other.__columns__ && 
          __columns__.all? { |name| 
            my_value = self[name]
            other_value = other[name]
            my_value == other_value }
      else
        super
      end
    end

    def [] name
      if @root.has_obj?(name)
        value_for(name)
      end
    end

    # Returns values of given keys as an array. 
    #
    def values_at *keys
      keys.map { |key| self[key] }
    end

    # Returns all column names. 
    #
    def __columns__
      @root.names
    end

    # Turns this row into a hash, performing deep conversion of all field names.
    # Use this to go into the Hash world and never come back. 
    #
    def to_h
      __columns__.map { |name| [name, value_for(name)] }.to_h
    end

  private 

    def value_for name
      if @attributes.has_key?(name)
        return @attributes[name]
      end

      node = @root[name]
      @attributes[name] = node && node.produce_value(@row_idx) { |node|
        Row.new(node, @row_idx)
      }
    end

    def cache_attribute name
      value = value_for(name)

      # Produce a local accessor via Virtus
      self.define_singleton_method(name) { value }

      value
    end
  end
end
