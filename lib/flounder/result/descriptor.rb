
module Flounder::Result
  class Descriptor
    include ::Flounder::PostgresUtils

    # Result obtained from the connection
    attr_reader :pg_result

    attr_reader :connection
  
    # Entity to use for field resolution
    attr_reader :entity

    # Root of the accessor tree. 
    # @api private
    attr_reader :accessor_root

    # @api private
    attr_reader :name_resolver

    def initialize connection, entity, pg_result, &name_resolver
      @entity = entity
      @pg_result = pg_result
      @connection = connection
      @name_resolver = name_resolver || -> (name) {}

      build_accessors
    end

    def row idx
      Row.new(accessor_root, idx)
    end

    def value type, row_idx, col_idx
      access_value(connection, pg_result, type, row_idx, col_idx)
    end

    # Parses and builds accessor structure for the result stored here. 
    #
    # @api private
    #
    def build_accessors
      @accessor_root = Accessor::Node.new
      each_field(entity, pg_result) do |idx, entity, field, type, binary|
        processed_entity, processed_name = name_resolver.call(field)
        entity = processed_entity if processed_entity
        field  = processed_name if processed_name

        # JOIN tables are available from the result using their singular names.
        if entity
          accessor_root[entity.singular].add_field(field, self, idx, type)
        end

        # The main entity and custom fields (AS something) are available on the
        # top-level of the result. 
        if field && (!entity || entity == self.entity)
          raise ::Flounder::DuplicateField, 
            "#{field.inspect} already defined in result set, aliasing occurs." \
            if accessor_root.has_obj? field

          accessor_root.add_field(field, self, idx, type)
        end

      end
    end
  end
end

require 'flounder/result/accessor/node'

