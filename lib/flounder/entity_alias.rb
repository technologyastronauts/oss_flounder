
module Flounder
  # Alias for an Entity, implementing roughly the same interface. 
  #
  # @see Entity
  #
  class EntityAlias
    def initialize entity, plural, singular
      @entity = entity
      @plural = plural
      @singular = singular
    end

    # Entity this alias refers to
    attr_reader :entity

    # Plural name of the alias
    attr_reader :plural

    # Singular name of the alias
    attr_reader :singular

    # Plural is also available as #name
    alias name plural   

    def table
      table = entity.table 
      table.alias(plural)
    end

    def column_names
      entity.column_names
    end

    def relations *a
      entity.relations *a
    end

    def fields *a
      a.map { |n| self[n] }
    end

    def inspect
      "<Flounder/Entity/Alias #{entity.name}(#{entity.table_name}) as (#{plural}, #{singular}))>"
    end
    alias to_s inspect


    def [] name
      Field.new(self, name, table[name])
    end
  end
end